import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        List<String> tabZKonsoli = new ArrayList<>();
        wypiszZawartoscListy(tabZKonsoli);

        boolean isWorking = true;
        while (isWorking) {
            System.out.println("Podaj komende: ");
            String komenda = scanner.nextLine();
            switch (komenda) {
                case "add":
                    System.out.println("Ile elementow chcesz dodac: ");
                    int ileElementow = scanner.nextInt();
                    dodaj(tabZKonsoli, ileElementow);
                    wypiszZawartoscListy(tabZKonsoli);
                    break;
                case "clear":
                    tabZKonsoli.clear();
                    break;
                case "printspace":
                    print();
                    break;
                case "collect":
                    Runtime.getRuntime().gc();// - wykonuje Garbage collection.
                    break;
                case "jakgleboko":
                    break;
                case "glebokosc":
                    System.out.println("Jak gleboko chcesz zejsc?" );
                    int jakGleboko = scanner.nextInt();
                    recurrentExecuteMethod(jakGleboko);
                default:
                    break;

            }
        }
    }

    public static void wypiszZawartoscListy(List<String> lista) {
        for (int i = 0; i < lista.size(); i++) {
            System.out.print(lista.get(i) + " ; ");
        }
        System.out.println();
    }

    public static void dodaj(List<String> tab, int ile) {
            String pom1 = scanner.nextLine();
        for (int i = 0; i < ile; i++) {
            System.out.println("Podaj " + (i + 1) + " element ktory chcesz dodac");
            String pom = scanner.nextLine();
            tab.add(pom);
        }
    }

    public static void recurrentExecuteMethod(int ileRazy) {
        if (ileRazy == 0) {
            print();
        } else
            recurrentExecuteMethod(ileRazy - 1);
    }

    public static void print() {
        Runtime.getRuntime().maxMemory();
        Runtime.getRuntime().freeMemory();// - wolna ogólnie do zaalokowania na same obiekty i zmienne
        Runtime.getRuntime().totalMemory(); //- pamięć całego procesu
        long usedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        long freeMemory = Runtime.getRuntime().maxMemory() - usedMemory;
        System.out.println("Used memory: " + usedMemory);
        System.out.println("Free memory: " + freeMemory);
    }
}